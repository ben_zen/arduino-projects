// Trying out a mocked keypad

// This is Windows-specific, so replace it once I move to an actual board.
#include <conio.h>
#include <string.h>

const char keys[4][3] = { { '1', '2', '3' },
                          { '4', '5', '6' },
                          { '7', '8', '9' },
                          { '*', '0', '#' } };

char *secret = "1234";
                        
// Waits for a keystroke; ignores function keys and arrows.
char get_key() {
  int read_in;
  read_in = _getch();
  if ((0 == read_in) || (0xE0 == read_in)) {
    _getch();
    read_in = 0;
  }
  return (char)read_in;
};

// Prints a star to the screen.
void print_star() {
  _putch('*');
}

// Checks that the PIN provided matches the secret stored in the file. Stupid-simple currently.
bool test_pin(const char *input_pin) {
  bool rv = false;
  rv = (strcmp(input_pin, secret) == 0) ? true : false;
  return rv;
};

bool run_pin_entry() {
  bool rv = false;
  char input_pin[5] = {0};
  _cputs("Enter the 4-digit PIN (or # to cancel):\n");
  int iter = 0;
  for (; iter < 4; iter++)
  {
    char readout = get_key();
    if ('#' == readout) {
      break;
    }
    input_pin[iter] = readout;
    print_star();
  }
  if (iter == 4) {
    rv = test_pin(input_pin);
  }
  // Remove the following when moving away from building for Windows.
  _putch('\n');
  return rv;
};

int main(int argv, char **argc) {
  bool testval = run_pin_entry();
  _cputs((testval) ? "Success!\n" : "Wrong PIN.\n"); // Clearly exiting via # is not failure in the same way. That's a bug.
};